package de.to10.cqlapi.modules;

import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.google.common.base.Function;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import de.to10.cqlapi.CqlApi;
import de.to10.cqlapi.exceptions.PluginNotLoadedException;
import de.to10.cqlapi.modules.utils.ModuleUtils;
import lombok.AccessLevel;
import lombok.Getter;

import java.util.Map;
import java.util.function.Consumer;

/**
 * @author vRchr
 */
public class CqlApiModule {
    @Getter( AccessLevel.PRIVATE )
    private static final ModuleUtils moduleUtils = new ModuleUtils( CqlApiModule.class.getSimpleName() );
    @Getter( AccessLevel.PRIVATE )
    private static Map<String, Session> sessions = Maps.newConcurrentMap();
    @Getter( AccessLevel.PRIVATE )
    private static Session masterSession;

    /**
     * Call Method to initialize Module.
     *
     * @param instance Plugin instance for configuration.
     */
    public static void init( CqlApi instance ) {
        //Define connection to CqlApi plugin.
        CqlApiModule.getSessions().putAll( instance.getCqlClass().getSessions() );
        CqlApiModule.masterSession = instance.getCqlClass().getMasterSession();

        //Set Module loaded.
        CqlApiModule.getModuleUtils().loadModule();
    }

    /**
     * Call Method to unload Module.
     */
    public static void unload( ) {
        CqlApiModule.getModuleUtils().unloadModule();
    }

    /**
     * Get Cassandra Session by defined Keyspace.
     *
     * @param keySpace for Session
     * @return Session
     * @throws PluginNotLoadedException if Module was already unloaded / not loaded.
     */
    public static Session getSession( String keySpace ) {
        //Check if module is loaded.
        CqlApiModule.getModuleUtils().checkAccess();

        //Return Session by defined Keyspace.
        return CqlApiModule.getSessions().get( keySpace.toLowerCase() );
    }

    /**
     * Build PreparedStatement with Master Session.
     *
     * @param cql Cql Query for Statement.
     * @return Result Statement.
     */
    public static PreparedStatement buildMasterStatement( String cql ) {
        //Check if module is loaded.
        CqlApiModule.getModuleUtils().checkAccess();

        return CqlApiModule.buildStatement( CqlApiModule.getMasterSession(), cql );
    }

    /**
     * Build PreparedStatement with defined Session.
     *
     * @param keySpace Cassandra KeySpace to get Session.
     * @param cql      Cql Query for Statement.
     * @return Result Statement.
     * @throws PluginNotLoadedException if Module was already unloaded / not loaded.
     */
    public static PreparedStatement buildStatement( String keySpace, String cql ) {
        //Check if module is loaded.
        CqlApiModule.getModuleUtils().checkAccess();

        return CqlApiModule.buildStatement( CqlApiModule.getSession( keySpace ), cql );
    }

    /**
     * Build PreparedStatement with defined Session.
     *
     * @param session Cassandra Session to build Statement.
     * @param cql     Cql Query for Statement.
     * @return Result Statement.
     */
    public static PreparedStatement buildStatement( Session session, String cql ) {
        //Check if module is loaded.
        CqlApiModule.getModuleUtils().checkAccess();
        return session.prepare( cql );
    }

    /**
     * Execute statement asynchronous in defined Keyspace.
     *
     * @param keySpace   Cassandra Keyspace to get Session.
     * @param statement  Cassandra Statement with Cql Query.
     * @param function   Function to transform ResultSet.
     * @param consumer   Consumer for result.
     * @param parameters Parameters to fill statement. (optional)
     * @param <T>        Type of Consumer result and result type of function.
     * @throws PluginNotLoadedException if Module was already unloaded / not loaded.
     */
    public static <T> void queryAsync( String keySpace, PreparedStatement statement, Function<ResultSet, T> function,
                                       final Consumer<T> consumer, Object... parameters ) {
        //Check if module is loaded.
        CqlApiModule.getModuleUtils().checkAccess();

        //Get Session by KeySpace; Execute with Session.
        CqlApiModule.queryAsync( CqlApiModule.getSessions().get( keySpace ), statement, function,
                consumer, parameters );
    }

    /**
     * Execute statement asynchronous in defined Session.
     *
     * @param session    Session to execute Statement.
     * @param statement  Cassandra Statement with Cql Query.
     * @param function   Function to transform ResultSet.
     * @param consumer   Consumer for result.
     * @param parameters Parameters to fill statement. (optional)
     * @param <T>        Type of Consumer result and result type of function.
     */
    public static <T> void queryAsync( Session session, PreparedStatement statement, Function<ResultSet, T> function,
                                       final Consumer<T> consumer, Object... parameters ) {
        //Check if module is loaded.
        CqlApiModule.getModuleUtils().checkAccess();

        //Bind parameters to Statement.
        BoundStatement boundStatement = statement.bind( parameters );
        //Define ResultSetFuture.
        ListenableFuture<ResultSet> resultSetFuture = session.executeAsync( boundStatement );
        //Transform ResultSetFuture to TFuture.
        ListenableFuture<T> future = Futures.transform( resultSetFuture, function );
        //Execute TFuture.
        Futures.addCallback( future, new FutureCallback<T>() {
            public void onSuccess( T result ) {
                //Accept Consumer with Result.
                consumer.accept( result );
            }

            public void onFailure( Throwable t ) {
                //Print Stacktrace.
                t.printStackTrace();
            }
        } );
    }

    /**
     * Execute statement asynchronous in defined Session.
     *
     * @param keySpace   Cassandra Keyspace to get Session.
     * @param statement  Cassandra Statement with Cql Query.
     * @param success    Runnable for successful update.
     * @param parameters Parameters to fill statement. (optional)
     * @throws PluginNotLoadedException if Module was already unloaded / not loaded.
     */
    public static void updateAsync( String keySpace, PreparedStatement statement, final Runnable success,
                                    Object... parameters ) {
        //Check if module is loaded.
        CqlApiModule.getModuleUtils().checkAccess();

        //Get Session by KeySpace; Execute with Session.
        CqlApiModule.updateAsync( CqlApiModule.getSessions().get( keySpace ), statement, success,
                parameters );
    }

    /**
     * Execute statement asynchronous in defined Session.
     *
     * @param session    Session to execute Statement.
     * @param statement  Cassandra Statement with Cql Query.
     * @param success    Runnable for successful update.
     * @param parameters Parameters to fill statement. (optional)
     */
    public static void updateAsync( Session session, PreparedStatement statement, final Runnable success,
                                    Object... parameters ) {
        //Check if module is loaded.
        CqlApiModule.getModuleUtils().checkAccess();

        //Bind parameters to Statement.
        BoundStatement boundStatement = statement.bind( parameters );
        //Define ResultSetFuture.
        ListenableFuture<ResultSet> resultSetFuture = session.executeAsync( boundStatement );
        //Execute ResultSetFutures.
        Futures.addCallback( resultSetFuture, new FutureCallback<ResultSet>() {
            public void onSuccess( ResultSet result ) {
                //Accept Runnable.
                success.run();
            }

            public void onFailure( Throwable t ) {
                //Print Stacktrace.
                t.printStackTrace();
            }
        } );

    }

}
