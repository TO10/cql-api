package de.to10.cqlapi.modules.utils;

import de.to10.cqlapi.exceptions.PluginNotLoadedException;
import lombok.Getter;

/**
 * @author vRchr
 */
@Getter
public class ModuleUtils {
    private final String MODULE_NAME;
    private ModuleStatus moduleStatus = ModuleStatus.NOT_LOADED;

    /**
     * Constructor of ModuleUtils.
     *
     * @param moduleName Name of Module.
     */
    public ModuleUtils( String moduleName ) {
        this.MODULE_NAME = moduleName;
    }

    /**
     * Call to set ModuleStatus to LOADED.
     */
    public void loadModule( ) {
        this.moduleStatus = ModuleStatus.LOADED;
    }

    /**
     * Call to set ModuleStatus to UNLOADED.
     */
    public void unloadModule( ) {
        this.moduleStatus = ModuleStatus.UNLOADED;
    }

    /**
     * Call to check access.
     */
    public void checkAccess( ) {
        try {
            if ( this.getModuleStatus() == ModuleStatus.NOT_LOADED )
                //Throw Exception.
                throw new PluginNotLoadedException( this.getMODULE_NAME() + " not loaded yet." );
            else if ( this.getModuleStatus() == ModuleStatus.UNLOADED )
                //Throw Exception
                throw new PluginNotLoadedException( this.getMODULE_NAME() + " already unloaded." );
        } catch ( PluginNotLoadedException e ) {
            e.printStackTrace();
        }
    }

}
