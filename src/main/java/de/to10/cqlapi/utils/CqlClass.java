package de.to10.cqlapi.utils;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.Session;
import com.google.common.collect.Maps;
import lombok.Getter;

import java.util.List;
import java.util.Map;

/**
 * @author vRchr
 */
@Getter
public class CqlClass {
    private String host;
    private Cluster cluster;
    private Session masterSession;
    private Map<String, Session> sessions = Maps.newConcurrentMap();

    /**
     * Define Contact Point for Cassandra.
     *
     * @param host
     */
    public CqlClass( String host ) {
        //Define Host.
        this.host = host;
    }

    /**
     * Connect Cluster and Master Session.
     */
    public void connect( ) {
        //Build Cluster.
        this.cluster = Cluster.builder().addContactPoint( this.getHost() ).build();
        //Connect Master Session.
        this.masterSession = this.getCluster().connect();
        //Connect all Sessions.
        this.initSessions();
    }

    /**
     * Connect all Sessions for all Keyspaces.
     */
    private void initSessions( ) {
        //Get all Keyspaces.
        List<KeyspaceMetadata> keySpaces = this.getCluster().getMetadata().getKeyspaces();
        //Iterates all Keyspaces.
        for ( KeyspaceMetadata keySpace : keySpaces ) {
            //Connect Session to Keyspace; Add to Sessions Map.
            this.getSessions().put( keySpace.getName().toLowerCase(), this.getCluster().connect( keySpace.getName() ) );
        }
    }

    /**
     * Get Cassandra Session by defined Keyspace. Create if not exists.
     *
     * @param keySpace for Session
     * @return Session
     */
    public Session getSession( String keySpace ) {
        //Get Session.
        Session session = this.getSessions().get( keySpace );

        //Session not exists.
        if ( session == null ) {
            //Create Keyspace.
            this.getMasterSession().execute( "CREATE KEYSPACE " + keySpace + " WITH REPLICATION = " +
                    "{'class':'SimpleStrategy', 'replication_factor' = 3};" );
            //Connect new Session to.
            session = this.getCluster().connect( keySpace );
        }

        //Return Session.
        return session;
    }

    /**
     * Close all Sessions, the Master Session and the Cluster.
     */
    public void disconnect( ) {
        //Iterate Sessions.
        for ( Session session : this.getSessions().values() ) {
            //Close iterated Session.
            session.close();
        }
        //Close Master Session.
        this.getMasterSession().close();
        //Close Cluster.
        this.getCluster().close();
    }
}
