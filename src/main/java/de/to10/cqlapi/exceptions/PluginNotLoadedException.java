package de.to10.cqlapi.exceptions;

/**
 * @author vRchr
 */
public class PluginNotLoadedException extends Exception {
    /**
     * Constructor for PluginNotLoadedException.
     */
    public PluginNotLoadedException() {
        super();
    }

    /**
     * Constructor with defined error for PluginNotLoadedException.
     *
     * @param trace
     */
    public PluginNotLoadedException( String trace) {
        super(trace);
    }
}
