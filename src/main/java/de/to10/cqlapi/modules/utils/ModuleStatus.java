package de.to10.cqlapi.modules.utils;

/**
 * @author vRchr
 */
public enum ModuleStatus {
    NOT_LOADED, LOADED, UNLOADED
}

