**Cassandra API**

Maven:

_Important:_

`<repository>
  <id>cassandra-repo</id>
  <url>https://mvnrepository.com/artifact/com.datastax.cassandra/cassandra-driver-core</url>
</repository>`
`<dependency>
  <groupId>com.datastax.cassandra</groupId>
  <artifactId>cassandra-driver-core</artifactId>
  <version>3.2.0</version>
</dependency>`

_Optional:_

`<repository>
  <id>lombok-repo</id>
  <url>https://mvnrepository.com/artifact/org.projectlombok/lombok</url>
</repository>`
`<dependency>
  <groupId>org.projectlombok</groupId>
  <artifactId>lombok</artifactId>
  <version>1.16.16</version>
</dependency>`


_Note: Install Lombok-Plugin in Intellij._




plugin.yml:

`depend: [CqlApi]`



Get Session by KeySpace:

`public static Session getSession( String keySpace ) throws PluginNotLoadedException`



Build PreparedStatement with KeySpace:

`public static PreparedStatement buildStatement( String keySpace, String cql )`



Build PreparedStatement with Session:

`public static PreparedStatement buildStatement( Session session, String cql )`



Build PreparedStatement with Master Session:

`public static PreparedStatement buildMasterStatement(String cql)`



Query with Keyspace:

`public static <T> void queryAsync( String keySpace, PreparedStatement statement, Function<ResultSet, T> function, final Consumer<T> consumer, Object... parameters )`
                                         


Query with Session:

`public static <T> void queryAsync( Session session, PreparedStatement statement, Function<ResultSet, T> function, final Consumer<T> consumer, Object... parameters )`
  
        
                                         
Update with Keyspace:

`public static void updateAsync( String keySpace, PreparedStatement statement, final Runnable success, Object... parameters )`
  
  
                                    
Update with Session:

`public static void updateAsync( Session session, PreparedStatement statement, final Runnable success, Object... parameters )`



_Function, etc. in JavaDoc. More features, etc. coming soon._ 


