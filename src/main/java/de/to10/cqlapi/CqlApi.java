package de.to10.cqlapi;

import de.to10.cqlapi.modules.CqlApiModule;
import de.to10.cqlapi.utils.CqlClass;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author vRchr
 */
@Getter
public class CqlApi extends JavaPlugin {
    private final String HOST = "127.0.0.1";
    private CqlClass cqlClass;

    /**
     * On Plugin load.
     */
    @Override
    public void onLoad( ) {
        //Load CqlClass.
        this.cqlClass = new CqlClass( this.getHOST() );
        //Connect CqlClass to Cassandra.
        this.getCqlClass().connect();

        //Initialize CqlApiModule.
        CqlApiModule.init( this );
    }

    /**
     * On Plugin disable.
     */
    @Override
    public void onDisable( ) {
        //Unload CqlApiModule
        CqlApiModule.unload();

        //Disable CqlClass.
        this.getCqlClass().disconnect();

        //Stop Server, to disable reloading.
        Bukkit.shutdown();
    }
}
